import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as appInsights from 'applicationinsights';
import { AzureApplicationInsightsLogger } from 'winston-azure-application-insights';
import winston from 'winston';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  console.log('before winston.add Azure');

  appInsights.setup('add57599-07a8-4851-ab3b-4a962f11aa03').start();

  winston.add(
    new AzureApplicationInsightsLogger({
      client: appInsights,
    }),
  );

  winston.error('winston.error');
  AzureApplicationInsightsLogger.error('azureLogger.error');
  console.error('eee');
  await app.listen(3000);
}
bootstrap();
